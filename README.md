# S&T Assignment

Python simple library with the proposed solution to the assignment given by Science & Technology to evaluate programming and software engineering skills.

The solution is a version of the *Connect Four* board game to be played in a Linux terminal using the keyboard to enter moves.

The game is played against the computer that only make random moves.

## Installation

To install this Python library please follow the following instructions:

### Clone the repository

```
git clone https://gitlab.com/jramirezm/s-and-t-assignment.git
```

### Create virtual environment and install

Create a Python 3 virtual environment using the tools most comfortable for you and then type:

```
cd s-and-t-assignment/
pip install -e .
```

This command should have installed all necessary dependences.

### Play the game

```
python app/play_connect_four.py
```

After this, the program ask you to select type of disc:

```
Enter type of disc (O/X, O starts the game):
```

And then an ASCII board should have being displayed in the terminal:

```
║ 1 ║ 2 ║ 3 ║ 4 ║ 5 ║ 6 ║ 7 ║
═════════════════════════════
║   ║   ║   ║   ║   ║   ║   ║
║   ║   ║   ║   ║   ║   ║   ║
║   ║   ║   ║   ║   ║   ║   ║
║   ║   ║   ║   ║   ║   ║   ║
║   ║   ║   ║   ║   ║   ║   ║
║   ║   ║   ║   ║   ║   ║   ║
╚═══════════════════════════╝

Enter column number [1, 2, 3, 4, 5, 6, 7]:
```

## Repository contents

* ```game```: folder with the core class of the *Connect Four* game.
* ```requirements```: folder with the details of the requiered Python packages for the game.
* ```test:```: Python scripts for testing and evaluating the code.
* ```app```: folder with the application to play the game.
* ```README.md```: this documentation file.
* ```setup.cfg```: file with useful aliases to simplify the testing procedure.
* ```setup.py```: Python script to install the library.
* ```VERSION```: file with the details of the library version.
