# file game.py
# brief Python class and functions to play Connect Four in a Linux terminal
# author Jaime Ramirez
# date Oct, 2022

from consoledraw import Console
import numpy as np
import random


class ConnectFour:

    def __init__(self, test=False):

        # Variables to keep track of game state
        # board: numpy array to store the position of the moves, value=1 for
        # the first player and value=-1 for the second player
        # num_discs: numpy array to store the number of discs of each column
        self.board = np.array([[0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0]])

        self.num_discs = np.array([0, 0, 0, 0, 0, 0, 0])

        # Avoid create drawing tools in testing mode
        self.test = test
        if not self.test:
            self.console = Console()
            self.col_numbers = "║ 1 ║ 2 ║ 3 ║ 4 ║ 5 ║ 6 ║ 7 ║"
            self.separator = "═════════════════════════════"
            self.disc_row = "║ {} ║ {} ║ {} ║ {} ║ {} ║ {} ║ {} ║"
            self.last_row = "╚═══════════════════════════╝"
            self.draw_board()

    def draw_board(self):

        # Change board values to symbols to keep tidy display
        display_dict = {-1: 'X', 1: 'O', 0: ' '}
        display = np.vectorize(display_dict.get)(self.board)

        # Draw display elements line by line
        self.console.clear()
        self.console.print(self.col_numbers)
        self.console.print(self.separator)
        for b in display:
            self.console.print(self.disc_row.format(*b))
        self.console.print(self.last_row)
        self.console.update()

    def update_board(self, col, value):

        # Update the board in the first available row in the selected column
        # and update the drawing only if not testing mode
        row = 5 - self.num_discs[col]
        self.board[row][col] = value
        self.num_discs[col] += 1
        if not self.test:
            self.draw_board()
        return (row, col)

    def check_winner(self, pos, value):

        # Checks all the possible conditions to win the game, extracting from
        # the board each sub array that contains the last played position and
        # check the sum of its elements in all relevant direction

        (row, col) = pos

        # Check winner in horizontal direction
        for slide in range(0, 4):
            low_lim = max(col - slide, 0)
            high_lim = min(low_lim + 4, 7)
            score = np.sum(self.board[row, low_lim:high_lim])
            if score == value:
                return True

        # Check winner in vertical direction
        for slide in range(0, 4):
            low_lim = max(row - slide, 0)
            high_lim = min(low_lim + 4, 6)
            score = np.sum(self.board[low_lim:high_lim, col])
            if score == value:
                return True

        # Check winner in diagonal direction
        for slide in range(0, 4):
            low_lim_row = max(row - slide, 0)
            high_lim_row = min(row - slide + 4, 6)
            low_lim_col = max(col - slide, 0)
            high_lim_col = min(col - slide + 4, 7)

            sub_board = self.board[low_lim_row:high_lim_row,
                                   low_lim_col:high_lim_col]
            if sub_board.shape != (4, 4):
                continue

            score = np.trace(sub_board)
            if score == value:
                return True

        # Check winner in anti-diagonal direction
        for slide in range(0, 4):
            low_lim_row = max(row - slide, 0)
            high_lim_row = min(row - slide + 4, 6)
            high_lim_col = min(col + slide + 1, 7)
            low_lim_col = max(col + slide + 1 - 4, 0)

            sub_board = self.board[low_lim_row:high_lim_row,
                                   low_lim_col:high_lim_col]
            if sub_board.shape != (4, 4):
                continue

            sub_board = np.fliplr(sub_board)
            score = np.trace(sub_board)
            if score == value:
                return True

        return False

    def available_columns(self):

        available = [i for i, v in enumerate(self.num_discs) if v < 6]
        return available

    def check_full_board(self):

        return True if sum(self.num_discs) == 42 else False


@staticmethod
def manual_move(available, game):

    disp = str(list(map(lambda x: x+1, available)))
    col = ''
    while True:
        col = input('Enter column number ' + disp + ': ')
        game.draw_board()
        if not col.isdigit():
            continue
        pos = int(col) - 1
        if pos in available:
            break

    return pos


@staticmethod
def automatic_move(available, _):

    return random.choice(available)
