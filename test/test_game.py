# file test-game.py
# brief Python script to test the ConnectFour game
# author Jaime Ramirez
# date Oct, 2022

from game.game import ConnectFour
from unittest import TestCase
import numpy as np


class TestResults(TestCase):

    def test_horizontal_wins(self):

        game = ConnectFour(test=True)

        game.board = np.array([[0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [1, 1, 1, 0, -1, -1, -1]])

        game.num_discs = np.array([1, 1, 1, 0, 1, 1, 1])

        pos = game.update_board(3, 1)
        self.assertTrue(game.check_winner(pos, 4),
                        'Player1 must win game')

        game.board = np.array([[0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0],
                               [1, 0, 0, 0, 0, 0, 0],
                               [1, 1, 1, 0, -1, -1, -1]])

        game.num_discs = np.array([2, 1, 1, 0, 1, 1, 1])

        pos = game.update_board(3, -1)
        self.assertTrue(game.check_winner(pos, -4),
                        'Player2 must win game')

    def test_vertical_wins(self):

        game = ConnectFour(test=True)

        game.board = np.array([[0, 0, 0, 0, 0, 0,  0],
                               [0, 0, 0, 0, 0, 0,  0],
                               [0, 0, 0, 0, 0, 0,  0],
                               [1, 0, 0, 0, 0, 0, -1],
                               [1, 0, 0, 0, 0, 0, -1],
                               [1, 0, 0, 0, 0, 0, -1]])

        game.num_discs = np.array([3, 0, 0, 0, 0, 0, 3])

        pos = game.update_board(0, 1)
        self.assertTrue(game.check_winner(pos, 4),
                        'Player1 must win game')

        game.board = np.array([[0, 0, 0, 0, 0, 0,  0],
                               [0, 0, 0, 0, 0, 0,  0],
                               [0, 0, 0, 0, 0, 0,  0],
                               [1, 0, 0, 0, 0, 0, -1],
                               [1, 0, 0, 0, 0, 0, -1],
                               [1, 1, 0, 0, 0, 0, -1]])

        game.num_discs = np.array([3, 1, 0, 0, 0, 0, 3])

        pos = game.update_board(6, -1)
        self.assertTrue(game.check_winner(pos, -4),
                        'Player2 must win game')

    def test_diagonal_wins(self):

        game = ConnectFour(test=True)

        game.board = np.array([[0,  0,  0,  0,  0, 0, 0],
                               [0,  0,  0,  0,  0, 0, 0],
                               [0,  0,  0,  0,  0, 0, 0],
                               [0,  0,  1,  1,  0, 0, 0],
                               [0,  1, -1, -1,  0, 0, 0],
                               [1, -1, -1,  1, -1, 0, 0]])

        game.num_discs = np.array([1, 2, 3, 3, 1, 0, 0])

        pos = game.update_board(3, 1)
        self.assertTrue(game.check_winner(pos, 4),
                        'Player1 must win game')

        game.board = np.array([[0,  0,  0,  0, 0, 0, 0],
                               [0,  0,  0,  0, 0, 0, 0],
                               [0,  0,  0,  0, 0, 0, 0],
                               [0,  0, -1,  1, 0, 0, 0],
                               [0, -1,  1, -1, 0, 0, 0],
                               [-1, 1,  1,  1, 0, 0, 0]])

        game.num_discs = np.array([1, 2, 3, 3, 0, 0, 0])

        pos = game.update_board(3, -1)
        self.assertTrue(game.check_winner(pos, -4),
                        'Player2 must win game')

    def test_antidiagonal_wins(self):

        game = ConnectFour(test=True)

        game.board = np.array([[0, 0, 0,  0,  0,  0,  0],
                               [0, 0, 0,  0,  0,  0,  0],
                               [0, 0, 0,  0,  0,  0,  0],
                               [0, 0, 0,  1,  1,  0,  0],
                               [0, 0, 0, -1,  1,  1, -1],
                               [0, 0, 0, -1, -1, -1,  1]])

        game.num_discs = np.array([0, 0, 0, 3, 3, 2, 2])

        pos = game.update_board(3, 1)
        self.assertTrue(game.check_winner(pos, 4),
                        'Player1 must win game')

        game.board = np.array([[0, 0,  0,  0,  0,  0, 0],
                               [0, 0,  0,  0,  0,  0, 0],
                               [0, 0,  0,  0,  0,  0, 0],
                               [0, 0,  1, -1,  0,  0, 0],
                               [0, 0, -1,  1, -1,  0, 0],
                               [0, 0,  1,  1,  1, -1, 0]])

        game.num_discs = np.array([0, 0, 3, 3, 2, 1, 0])

        pos = game.update_board(2, -1)
        self.assertTrue(game.check_winner(pos, -4),
                        'Player2 must win game')

    def test_draw(self):

        game = ConnectFour(test=True)

        game.board = np.array([[1,  1,  1, -1,  1,  1,  0],
                               [-1, -1, -1,  1, -1, -1, -1],
                               [1,  1, -1, -1, -1,  1, -1],
                               [-1, -1,  1,  1, -1, -1,  1],
                               [1,  1, -1, -1,  1,  1, -1],
                               [1,  1, -1, -1,  1, -1,  1]])

        game.num_discs = np.array([6, 6, 6, 6, 6, 6, 5])

        pos = game.update_board(6, 1)
        self.assertFalse(game.check_winner(pos, 4),
                         'Game must be a draw')
