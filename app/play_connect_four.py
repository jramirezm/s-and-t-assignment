# file play_connect_four.py
# brief Python script to play the game using the package
# author Jaime Ramirez
# date Oct, 2022

from game.game import ConnectFour, manual_move, automatic_move


if __name__ == '__main__':

    discs = ''
    while discs not in ['O', 'X']:
        discs = input('Enter type of disc (O/X, O starts the game): ').upper()

    # Select order of the players according to the disc selected by the user
    if discs == 'O':
        move1 = manual_move
        move2 = automatic_move
    else:
        move1 = automatic_move
        move2 = manual_move

    game = ConnectFour()

    while not game.check_full_board():

        # Both players share the same logic:
        # - First check which column is available
        # - Then ask to the player to select one move
        # - Update the board to display this move
        # - Check if is a winner move

        columns = game.available_columns()
        col = move1(columns, game)
        pos = game.update_board(col, 1)
        winner = game.check_winner(pos, 4)
        if winner:
            print('Player 1 win!')
            exit()

        columns = game.available_columns()
        col = move2(columns, game)
        pos = game.update_board(col, -1)
        winner = game.check_winner(pos, -4)
        if winner:
            print('Player 2 win!')
            exit()

    print('Game is a draw')
