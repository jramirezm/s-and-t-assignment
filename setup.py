import os
import codecs
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))


def read(*folders):
    with codecs.open(os.path.join(here, *folders), encoding='utf-8') as fd:
        return fd.read()


def get_requirements(file_name):
    requires_file = read('requirements', file_name)
    return requires_file.splitlines()


def find_version():
    with open("VERSION", "r") as fd:
        return fd.read().rstrip()


setup(
    name='Connect Four Game',

    version=find_version(),

    description='Simple program to play Connect Four in a Linux terminal.',
    long_description=read('README.md'),

    url='',

    author='Jaime Ramirez',
    author_email='jramirezm@gmail.com',

    license='',

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: Linux',
        'Topic :: Software Development :: Gaming',
        'Programming Language :: Python',
    ],

    keywords='Science Technology Game Connect Four',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=get_requirements('default.txt'),
    test_suite='test',
    include_package_data=True,
    extras_require={},
    package_data={},
    data_files=[]
)
